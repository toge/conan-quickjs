import os

from conan import ConanFile
from conan import tools
from conan.tools.cmake import CMakeToolchain, CMake, cmake_layout

class QuickJSConan(ConanFile):
    name = "quickjs"
    description = "QuickJS is a small and embeddable Javascript engine."
    license = "MIT"
    url = "https://github.com/conan-io/conan-center-index"
    homepage = "https://bellard.org/quickjs/"
    settings = "os", "arch", "compiler", "build_type"
    options = {
        "shared": [True, False],
        "fPIC": [True, False],
        "use_bignum": [True, False], 
        "dump_leaks": [True, False],
    }
    default_options = {
        "shared": False,
        "fPIC": True,
        "use_bignum" : True, 
        "dump_leaks": False,
    }

    @property
    def _source_subfolder(self):
        return "source_subfolder"

    def export_sources(self):
        tools.files.copy(self, "CMakeLists.txt", self.recipe_folder, self.export_sources_folder)

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def configure(self):
        if self.options.shared:
            del self.options.fPIC
        del self.settings.compiler.libcxx
        del self.settings.compiler.cppstd

    def source(self):
        tools.files.get(self, **self.conan_data["sources"][self.version], strip_root=True, destination=self._source_subfolder)

    def generate(self):
        toolchain = CMakeToolchain(self)
        toolchain.variables["USE_BIGNUM"] = self.options.use_bignum
        toolchain.variables["DUMP_LEAKS"] = self.options.dump_leaks
        toolchain.generate()

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        tools.files.copy(self, "LICENSE", os.path.join(self.source_folder, self._source_subfolder), os.path.join(self.package_folder, "licenses"))
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["quickjs"]

        if self.options.use_bignum == True:
            self.cpp_info.defines.append("CONFIG_BIGNUM")

        if self.settings.os in ["Linux", "FreeBSD"]:
            self.cpp_info.system_libs.append("dl")
            self.cpp_info.system_libs.append("m")
