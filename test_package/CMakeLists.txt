cmake_minimum_required(VERSION 3.0.0)
project(test_package C)

find_package(quickjs REQUIRED CONFIG)

add_executable(${PROJECT_NAME} ${PROJECT_NAME}.c)
target_link_libraries(${PROJECT_NAME} quickjs::quickjs)
